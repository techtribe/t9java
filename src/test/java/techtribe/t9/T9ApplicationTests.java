package techtribe.t9;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import techtribe.t9.T9Application;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = T9Application.class)
@WebAppConfiguration
public class T9ApplicationTests {

	@Test
	public void contextLoads() {
	}

}
