package techtribe.t9.data.dto;

public class Response {
	private int status;
	private StatusCode statusCode;
	private String exception;
	
	public Response() {
		status = 200;
		statusCode = StatusCode.SUCCESS;
		exception = "";
	}
	
	public StatusCode getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(StatusCode statusCode) {
		this.statusCode = statusCode;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public static enum StatusCode {
		SUCCESS,FAILURE
	}
}
