package techtribe.t9.data.model;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class DictionaryNode {

	private DictionaryNode[] children;
	private Set<String> words;

	public DictionaryNode() {
		children = new DictionaryNode[10];
	}

	public DictionaryNode(int numNodes) {
		children = new DictionaryNode[numNodes];
	}

	public DictionaryNode getChild(int index) {
		return children[index];
	}

	@Override
	public String toString() {
		return "{Words:" + words + ", children:"
				+ Arrays.deepToString(children) + "}";
	}

	public void addWordAtNode(int[] t9, String word) {
		DictionaryNode currentNode = this;
		currentNode = createNodes(t9);
		if (currentNode.words == null) {
			currentNode.words = new TreeSet<String>();
		}
		currentNode.words.add(word);
	}

	private DictionaryNode createNodes(int[] t9) {
		DictionaryNode currentNode = this;
		for (int i = 0; i < t9.length; i++) {
			if (currentNode.children == null) {
				currentNode.children = new DictionaryNode[10];
			}
			if (currentNode.getChild(t9[i]) == null) {
				currentNode.children[t9[i]] = new DictionaryNode();
			}
			currentNode = currentNode.getChild(t9[i]);
		}
		return currentNode;
	}
	
	public int getIndexForT9(int[] t9Mappings, String value) {
        DictionaryNode currentNode = this;
        for (int i = 0; i < t9Mappings.length; i++) {
            currentNode = currentNode.getChild(t9Mappings[i]);
        }
        if (currentNode.words != null) {
            return getIndex(currentNode.words, value);
        } else {
            throw new RuntimeException("Invalid word.. not in the dictionary");
        }
    }

    private int getIndex(Set<String> set, Object value) {
        int result = 0;
        for (Object entry : set) {
            if (entry.equals(value)) return result;
            result++;
        }
        throw new RuntimeException("Invalid word.. not in the dictionary");
    }
}