package techtribe.t9.data.model;

public class DictionaryContextHolder {
    DictionaryNode root;
    T9Mapping mappings;

    public DictionaryContextHolder(DictionaryNode root, T9Mapping mappings) {
		this.root = root;
		this.mappings = mappings;
	}

	public T9Mapping getMappings() {
        return mappings;
    }

    public DictionaryNode getRoot() {
        return root;
    }

}
