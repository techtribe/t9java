package techtribe.t9.data.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class T9Mapping {

	private Map<Character,Character> map;
	private char space;
	private char next;
	
	/**
	 * Takes the key config like { '2' -> ['a','b','c'] , '#' -> [' ']  } 
	 * @param config
	 */
	public T9Mapping(Map<String,String> config) {
		map = new HashMap<>();
		for(Entry<String, String> entry : config.entrySet()) {

			char[] letters = entry.getValue().toCharArray();
			for(Character letter : letters)
			{
				Character key = entry.getKey().charAt(0);
				if(letter == ' ')
				{
					space = key;
				}
				else if(letter == '*')
				{
					next = key;
				}
				else
				{
					Character oldKey = map.put(letter, key);
					if(oldKey != null)
					{
						throw new IllegalArgumentException("Same letter "+letter+" has more than one mappings "+key+" and "+oldKey);
					}
				}
			}
		}
	}
	
	public int getNumberForLetter(char letter) {
		return map.get(letter);
	}
	
	public int[] getT9ForWord(String word) {
		int[] t9 = new int[word.length()];
		for(int i=0;i<word.length();i++)
		{
			t9[i] = getNumberForLetter(word.charAt(i)) - '0';
		}
		return t9;
	}
	
	public char getSpaceCharater()
	{
		return space;
	}
	
	public char getNextCharater()
	{
		return next;
	}
}
