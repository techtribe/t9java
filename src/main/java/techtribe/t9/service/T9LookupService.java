package techtribe.t9.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import techtribe.t9.data.model.DictionaryContextHolder;
import techtribe.t9.data.model.DictionaryNode;
import techtribe.t9.data.model.T9Mapping;
import techtribe.t9.repository.T9Repository;

@Service
public class T9LookupService {

    @Autowired
    T9Repository repository;
    
    public List<String> calculateT9(ArrayList<String> messages) {
        List<String> t9HashList = new ArrayList<String>();
        DictionaryContextHolder dictionary = repository.getDictionary();
        T9Mapping t9Mappings = dictionary.getMappings();
        
        for (String message : messages) {
            String[] words = message.split(" ");
            String[] t9Hash = new String[words.length];
            for (int i = 0; i < words.length; i++) {
            	if(words[i].length() == 0)
            	{
            		t9Hash[i] = "";
            	}else{
            		t9Hash[i] = calculateT9ForWord(dictionary, words[i]);
            	}
                
            }
            String finalT9 = String.join(String.valueOf(t9Mappings.getSpaceCharater()), t9Hash);
            t9HashList.add(finalT9);
        }
        return t9HashList;
    }

	public String calculateT9ForWord(String word) {
		return calculateT9ForWord(repository.getDictionary(), word);
	}
	
    private String calculateT9ForWord(DictionaryContextHolder dictionary, String word) {
        
        if (dictionary == null || dictionary.getMappings() == null) {
            throw new RuntimeException("No dictionary configured");
        }
        T9Mapping mappings = dictionary.getMappings();
        
        DictionaryNode dictionaryRoot = dictionary.getRoot();
        
        int[] t9 = mappings.getT9ForWord(word);
        int index = dictionaryRoot.getIndexForT9(t9, word);

        StringBuilder builder = new StringBuilder();
        for (int digit : t9) {
            builder.append(digit);
        }
        for (int i = 0; i < index; i++) {
            builder.append(mappings.getNextCharater());
        }

        return builder.toString();
    }

}
