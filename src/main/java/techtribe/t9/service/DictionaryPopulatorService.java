package techtribe.t9.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import techtribe.t9.data.model.DictionaryContextHolder;
import techtribe.t9.data.model.DictionaryNode;
import techtribe.t9.data.model.T9Mapping;
import techtribe.t9.repository.T9Repository;

@Service
public class DictionaryPopulatorService {
    
	@Autowired
    T9Repository repository;
    
    @Value("${dictionary.filePath}")
    String dictionaryfilePath;

    public void populate(Map<String, String> config) throws IOException {
        try(BufferedReader reader = new BufferedReader(new FileReader(dictionaryfilePath)))
        {
	        DictionaryNode node = new DictionaryNode();
	        T9Mapping mappings = new T9Mapping(config);
	        String value = null;
	        while ((value = reader.readLine()) != null) {
	            addWord(value, node, mappings);
	        }
	        DictionaryContextHolder ctx = new DictionaryContextHolder(node, mappings);
	        repository.setDictionary(ctx);
        }
    }
    
    public void addWord(String word) {
    	DictionaryContextHolder dictionary = repository.getDictionary();
    	addWord(word, dictionary.getRoot(), dictionary.getMappings());
    }
    
    private void addWord(String word, DictionaryNode root, T9Mapping t9Mappings) {
        int[] t9 = t9Mappings.getT9ForWord(word);
        root.addWordAtNode(t9, word);
    }

}
