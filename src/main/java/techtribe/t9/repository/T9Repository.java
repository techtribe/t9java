package techtribe.t9.repository;

import org.springframework.stereotype.Repository;

import techtribe.t9.data.model.DictionaryContextHolder;

@Repository
public class T9Repository {
	DictionaryContextHolder currentDictionary;

	public DictionaryContextHolder getDictionary() {
		return currentDictionary;
	}

	public void setDictionary(DictionaryContextHolder currentDictionary) {
		this.currentDictionary = currentDictionary;
	}
}
