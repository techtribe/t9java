package techtribe.t9.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import techtribe.t9.data.dto.Response;
import techtribe.t9.data.dto.Response.StatusCode;
import techtribe.t9.service.DictionaryPopulatorService;
import techtribe.t9.service.T9LookupService;

@RestController
public class T9Controller {
    
	@Autowired
    private T9LookupService service;

    @Autowired
    DictionaryPopulatorService populator;

    @RequestMapping("/getT9Hash")
    public @ResponseBody List<String> getT9Hash(@RequestBody ArrayList<String> messages){
        return service.calculateT9(messages);
    }

    @RequestMapping(value="/populateDict", produces="application/json")
    public @ResponseBody Response populateDict(@RequestBody HashMap<String, String> config) throws IOException{
        
    	try{
            populator.populate(config);
            return new Response();
        }catch(IOException ex){
            Response response = new Response();
            response.setStatusCode(StatusCode.FAILURE);
            response.setException(ex.getLocalizedMessage());
            return response;
        }
    }
}