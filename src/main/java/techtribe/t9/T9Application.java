package techtribe.t9;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T9Application {
	public static void main(String[] args) {
		SpringApplication.run(T9Application.class, args);
	}
}
