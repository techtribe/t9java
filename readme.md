# T9 Dictionary

## For Running
### Service Information
Runs on port: **8080**.
Request samples attached as **t9.postman_collection.json**
### Note
The dictionary words file used for running this is present as **dictionary.txt**.
For running the application please move the file to the path configured in **application.properties**

## Problem Statement
[T9 Dictionary](/t9_problem1.pdf)